import cmd, os, re

from Poker import Poker

class PokerCmd(cmd.Cmd):

	prompt = "Poker> "

	def __init__(self, poker):
		super().__init__()
		self.poker = poker

	#Command for setting opponents
	def do_opponents(self, arg):
		'o (opponents) <int 1-22> \t- Sets the number of opponents'

		try:
			num = int(arg)
			if num < 1 or num > 22:
				raise ValueError()

		except ValueError:
			print('SYNTAX ERR: o (opponents) <int 1-22> \t- Sets the number of opponents')
			return

		self.poker.opponents = num
		print('Successfully set opponents to ' + str(num))


	def do_o(self, arg):
		'o (opponents) <int 1-22> \t- Sets the number of opponents'
		self.do_opponents(arg)

	#Command for setting the starting hand
	def do_hand(self, arg):
		'h (hand) <string 1-9|T|J|Q|K|A + S|C|H|D> <string 1-9|T|J|Q|K|A + S|C|H|D>\n\t-Set the starting hand eample (7 of hearts and king of spades): hand 7H KS'

		try:
			if not arg:
				raise ValueError()

			args = arg.upper().split(' ')

			if len(args) != 2:
				raise ValueError()
			if len(args[0]) != 2:
				raise ValueError()
			if len(args[1]) != 2:
				raise ValueError()
			if re.match('[0-9,T,J,Q,K,A][S,C,H,D]', args[0]) == None:
				raise ValueError()
			if re.match('[0-9,T,J,Q,K,A][S,C,H,D]', args[1]) == None:
				raise ValueError()

		except ValueError:
			print('SYNTAX ERR: h (hand) <string 1-9|T|J|Q|K|A + S|C|H|D> <string 1-9|T|J|Q|K|A + S|C|H|D>\n\t-Set the starting hand example (7 of hearts and king of spades): hand 7H KS')
			return

		if args[0] == args[1]:
			print('You can not have two of the same card in your starting hand')
			return

		self.poker.hand = args
		print('Successfully set the starting hand to ' + args[0] + ' ' + args[1])

	def do_h(self, arg):
		'h (hand) <string 1-9|T|J|Q|K|A + S|C|H|D> <string 1-9|T|J|Q|K|A + S|C|H|D>\n\t-Set the starting hand eample (7 of hearts and king of spades): hand 7H KS'
		self.do_hand(arg)

	#Command for setting where to start
	def do_start(self, arg):
		's (start) <F(FLOP) | T(TURN) | R(RIVER)\t-Set where to start'

		arg = arg.upper()

		if arg == 'F' or arg == 'FLOP':
			self.poker.start = 0
			print('Successfully set start to before the flop')
		elif arg == 'T' or arg == 'TURN':
			self.poker.start = 1
			print('Successfully set start to before the turn')
		elif arg == 'R' or arg == 'RIVER':
			self.poker.start = 2
			print('Successfully set start to before the river')
		else:
			print('SYNTAX ERR: s (start) <F(FLOP) | T(TURN) | R(RIVER)\t-Set where to start')

	def do_s(self, arg):
		's (start) <F(FLOP) | T(TURN) | R(RIVER)\t-Set where to start'
		self.do_start(arg)

	#Command for displaying info
	def do_info(self, arg):

		'i (info) \t- Displays info about the game'

		start = [
			'FLOP',
			'TURN',
			'RIVER'
		]

		print(
			  '\n'
			+ '+-----------------------------------------------+\n'
			+ '| INFO                                          |\n'
			+ '+-----------------------------------------------+\n\n'
			+ 'Opponents:\t' + str(self.poker.opponents) + '\n'
			+ 'Hand:\t\t' + self.poker.hand[0] + ' ' + self.poker.hand[1] + '\n'
			+ 'Start:\t\t' + start[self.poker.start] + '\n'
		)
	
	def do_i(self, arg):
		'i (info) \t- Displays info about the game'
		self.do_info(arg)

	#Command for clearing the terminal
	def do_clear(self, arg):
		'c (clear) \t- Clears the screen'
		os.system('cls' if os.name == 'nt' else 'clear')

	def do_c(self, arg):
		'c (clear) \t- Clears the screen'
		self.do_clear(arg)
